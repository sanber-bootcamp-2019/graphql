import { Server } from 'net';
import 'source-map-support/register';
import { startServer } from './server';
import { syncDatabase, closeDatabase } from './modules/orm';

/**
 * Arkana customer api servers
 *
 * @export
 * @class App
 */
export class App {
  private _server: Server;

  /**
   * Creates an instance of App.
   * @param {boolean} [isTest=false]
   * @memberof App
   */
  constructor(private isTest: boolean = false) {}

  /**
   * start app
   *
   * @memberof App
   */
  async start() {
    await syncDatabase({ force: this.isTest });
    this._server = await startServer();
  }

  /**
   * stop app
   *
   * @memberof App
   */
  async stop() {
    await closeDatabase();

    await new Promise((resolve, reject) => {
      this._server.close((err) => {
        if (err) {
          return reject(err);
        }
        if (!this.isTest) {
          /* tslint:disable */
          console.log('server stopped');
          /* tslint:enable */
        }
        resolve();
      });
    });
  }
}
